import React, {Component, Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as recipesActions from '../../entities/recipesActions';

import RecipeItem from '../../components/recipeItem/RecipeItem';

export class RecipesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: []
        };
    }

    componentDidMount() {
        this.props.actions.fetchAllRecipes();

        this.setState({recipes: this.props.recipes});
    }

    render() {
        const recipesList = this.props.recipes.recipes;
        return (
            <Fragment>
                {recipesList.map(item => <RecipeItem id={item.id} title={item.title} dateCreated={item.dateCreated} />)}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        recipes: state.recipes
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...recipesActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipesList);
