import React, {Component} from 'react';
import {Switch} from 'react-router-dom';
import {Route} from 'react-router';

import Home from "./scenes/home/Home";
import AddRecipe from "./scenes/addRecipe/AddRecipe";
import EditRecipe from "./scenes/editRecipe/EditRecipe";
import RecipeDetails from "./scenes/recipeDetails/RecipeDetails";

import './App.module.less';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/home" component={Home}/>
            <Route path="/edit/:id" component={EditRecipe}/>
            <Route path="/add-recipe/:id" component={AddRecipe}/>
            <Route path="/add-recipe" component={AddRecipe}/>
            <Route path="/details/:id" component={RecipeDetails}/>
        </Switch>
      </div>
    );
  }
}

export default App;
