import {createAction} from 'redux-actions';
import axios from 'axios';

export const fetchAll = createAction('FETCH_ALL');
export const addRecipe = createAction('ADD_RECIPE');
export const getRecipe = createAction('GET_RECIPE');
export const editRecipe = createAction('EDIT_RECIPE');
export const deleteRecipe = createAction('DELETE_RECIPE');

export function fetchAllRecipes() {
    return dispatch => {
        axios.get('http://localhost:8000/recipes')
            .then(response => dispatch(fetchAll(response.data)));
    }
}

export function addNewRecipe(recipe) {
    return dispatch => {
        return axios.post('http://localhost:8000/recipes', recipe)
            .then(response => dispatch(addRecipe(response)))
            .catch(error => dispatch(console.log(error)));

    }
}

export function getCurrentRecipe(id) {
    return dispatch => {
        return axios.get(`http://localhost:8000/recipes/${id}`)
            .then(response => dispatch(getRecipe(response.data)));
    }
}

export function editCurrentRecipe(id, title, description) {
    return dispatch => {
        const editedRecipe = {};
        editedRecipe.title = title;
        editedRecipe.description = description;

        return axios.put(`http://localhost:8000/recipes/${id}`, editedRecipe)
            .then(response => dispatch(editRecipe(response)))
            .catch(error => dispatch(console.log(error)));

    }
}

export function deleteCurrentRecipe(id) {
    return dispatch => {
        const postUrl = `http://localhost:8000/recipes/${id}`;
        return axios.delete(postUrl)
            .then(response => dispatch(deleteRecipe(response.data)));
    }
}
