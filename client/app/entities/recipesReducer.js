import { handleActions } from 'redux-actions';
import { fetchAll, addRecipe, getRecipe, editRecipe, deleteRecipe } from "./recipesActions";

export const initialState = {
    recipes: [],
    currentRecipe: {}
};

export default handleActions(
    {
        [fetchAll]: (state, action) => {
            return Object.assign({}, state, {
                recipes: action.payload
            })
        },
        [addRecipe]: state => {
            return state
        },
        [getRecipe]: (state, action) => {
            return Object.assign({}, state, {
                currentRecipe: action.payload
            })
        },
        [editRecipe]: (state, action) => {
            return Object.assign({}, state, {
                currentRecipe: action.payload
            })
        },
        [deleteRecipe]: (state, action) => {
            return Object.assign({}, state, {
                recipes: action.payload
            })
        }
    },
    initialState
)
