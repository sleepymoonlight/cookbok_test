import React from 'react';
import PropTypes from 'prop-types';

import style from './input.module.less';

const Input = ({description, types, value, onChange, placeholder}) => (
    <label className={style.label}>
        <p className={style.text}>{description}</p>
        {
            types
                ? <textarea
                    value={value}
                    onChange={onChange}
                    placeholder={placeholder}
                />
                : <input
                    value={value}
                    type='text'
                    onChange={onChange}
                    placeholder={placeholder}
                />
        }
    </label>
);

Input.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
};

export default Input;
