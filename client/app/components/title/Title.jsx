import React from 'react';

import styles from './title.module.less'

const Title = props => (
    <div className={styles.title}>
        {props.children}
    </div>
);

export default Title;
