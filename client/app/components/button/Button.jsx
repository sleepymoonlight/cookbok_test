import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/es/Link';

import styles from './button.module.less';

const Button = ({href, isLarge = false, children}) => {
    const sizeClass = isLarge ? 'largeButton' : 'smallButton';

    return (
        href
            ? <Link to={href} className={`${styles.button} ${sizeClass}`}>{children}</Link>
            : <button className={`${styles.button} ${sizeClass}`}>{children}</button>
    );
};

Button.propTypes = {
    href: PropTypes.string,
    size: PropTypes.bool
};

export default Button;
