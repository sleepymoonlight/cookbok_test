import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import styles from './recipeItem.module.less';

const RecipeItem = ({id, title, dateCreated}) => (
    <Link to={`/details/${id}`} style={{textDecoration: 'none', color: 'black'}}>
        <div className={styles.recipe}>
            <div className={styles.recipe__title}>{title}</div>
            <div className={styles.recipe__date}>Created: {dateCreated}</div>
        </div>
    </Link>
);

RecipeItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    dateCreated: PropTypes.string.isRequired
};

export default RecipeItem;
