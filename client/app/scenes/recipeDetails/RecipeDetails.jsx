import React from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import {getCurrentRecipe, deleteCurrentRecipe} from '../../entities/recipesActions';

import Button from '../../components/button/Button';

import styles from './recipeDetails.module.less';
import RecipeItem from "../../components/recipeItem/RecipeItem";

class RecipeDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipe: {},
            redirect: false,
            changelogDisplay: false,
            descriptionDisplay: false
        }
    }

    componentDidMount() {
        this.props.getCurrentRecipe(this.props.match.params.id)
            .then(recipe => this.setState({recipe: recipe.payload}));
    }

    componentDidUpdate(prevProps) {
        const prevId = prevProps.match.params.id;
        const id = this.props.match.params.id;

        if (prevId !== id) {
            this.props.getCurrentRecipe(id)
                .then(recipe => this.setState({recipe: recipe.payload}));
        }
    }

    deleteRecipe = id => {
        this.props.deleteCurrentRecipe(id)
            .then(recipe => this.setState({redirect: true}));
    };

    toggleChangeLog = () => {
        this.setState(currentState => ({
            changelogDisplay: !currentState.changelogDisplay,
        }));
    };

    toggleDescription = id => {
        this.setState({activePrevDescription: id});
        console.log(id);
    };

    render() {
        const {id, title, description, dateChanged, dateCreated, childrenRecipes, previousVersions} = this.state.recipe;

        return (
            <div className={styles.box}>
                <div className={styles.container}>
                    <div className={styles.backButton}>
                        <Button href="/">&#8592; back to list</Button>
                    </div>
                    <div className={styles.content}>
                        <div className={styles.title}>
                            {title}
                        </div>
                        <div className={styles.description}>
                            {description}
                        </div>
                        <div className={styles.date}>
                            <div>Created: {dateCreated}</div>
                            <div>Changed: {dateChanged}</div>
                        </div>
                    </div>
                    <div className={styles.actions}>
                        <div>
                            <Button href={`/edit/${id}`}>Edit recipe</Button>
                        </div>
                        <div onClick={this.deleteRecipe.bind(this, id)}>
                            <Button>Delete recipe</Button>
                        </div>
                    </div>
                    {
                        childrenRecipes
                        && childrenRecipes.length > 0
                        && <div className={styles.childrenRecipes}>
                            *Based on this recipes*:
                            <div className={styles.childrenRecipes__list}>
                                {
                                    childrenRecipes.map(rec => (
                                        <RecipeItem id={rec.id} title={rec.title} dateCreated={rec.dateCreated}/>
                                    ))
                                }
                            </div>
                        </div>
                    }
                    <div className={styles.addNew}>
                        <Button href={`/add-recipe/${id}`} isLarge={true}>
                            + add another child recipe
                        </Button>
                    </div>
                    <div className={styles.previousVersion}>
                        <div className={styles.changelogButton} onClick={() => this.toggleChangeLog()}>
                            &#8595; Change log
                        </div>
                        {
                            previousVersions
                            && previousVersions.length > 0
                            && this.state.changelogDisplay
                            && previousVersions.map((prev, index) => (
                                <>
                                    <div className={styles.previousVersion__title}
                                         onClick={() => this.toggleDescription(prev.id)}
                                         key={prev.id}
                                    >
                                        <div>{`${index + 1}. ${prev.title}`}</div>
                                        <div>Created: {prev.dateCreated}</div>
                                    </div>
                                    <div style={{
                                        display: this.state.activePrevDescription === prev.id ? 'block' : 'none',
                                        paddingBottom: '10px'
                                    }}>
                                        {prev.description}
                                    </div>
                                </>
                            ))
                        }
                    </div>
                </div>
                {this.state.redirect && (<Redirect to={'/'}/>)}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        recipe: state.recipe
    };
}

export default connect(mapStateToProps, {getCurrentRecipe, deleteCurrentRecipe})(RecipeDetails);
