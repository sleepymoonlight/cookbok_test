import React from 'react';
import style from './editRecipe.module.less';
import Title from "../../components/title/Title";
import RecipeForm from "../../features/recipeForm/recipeForm";
import {bindActionCreators} from "redux";
import * as RecipesActions from "../../entities/recipesActions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

class EditRecipe extends React.Component {
    state = {
        redirect: false
    };

    onSubmit = (title, description) => {
        this.props.actions.editCurrentRecipe(this.props.match.params.id, title, description);

        this.setState({redirect: true});
    };

    render() {
        return (
            <div className={style.box}>
                <div className={style.container}>
                    <Title>Edit recipe</Title>
                    <RecipeForm
                        onSubmit={(title, description) => this.onSubmit(title, description)}
                        title={this.props.currentRecipe.title}
                        description={this.props.currentRecipe.description}
                    />
                </div>
                {this.state.redirect && (
                    <Redirect to={`/details/${this.props.match.params.id}`}/>
                )}
            </div>
        )
    }
};

function mapStateToProps(state) {
    return {
        currentRecipe: state.recipes.currentRecipe,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...RecipesActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditRecipe);
