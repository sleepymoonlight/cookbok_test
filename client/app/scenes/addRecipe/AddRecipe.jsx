import React from 'react';
import style from './addRecipe.module.less';
import Title from "../../components/title/Title";
import RecipeForm from "../../features/recipeForm/recipeForm";
import {bindActionCreators} from "redux";
import * as RecipesActions from "../../entities/recipesActions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

class AddRecipe extends React.Component {
    state = {
        redirect: false
    };

    onSubmit = (title, description) => {
        if (this.state.text !== '' && this.state.description !== '') {
            let newRecipe = {
                title: title,
                description: description
            };

            if (this.props.match.params.id) {
                newRecipe.childOf = this.props.match.params.id;
            }

            this.props.actions.addNewRecipe(newRecipe);
        }

        this.setState({redirect: true});
    };

    render() {
        return (
            <div className={style.box}>
                <div className={style.container}>
                    <Title>Add new recipe</Title>
                    <RecipeForm
                        onSubmit={(title, description) => this.onSubmit(title, description)}
                        title=''
                        description=''
                    />
                </div>
                {this.state.redirect && (
                    <Redirect to={`/`}/>
                )}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        currentRecipe: state.recipes.currentRecipe,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...RecipesActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRecipe);
