import React from 'react';

import RecipesList from '../../features/recipesList/RecipesList.jsx';
import Title from '../../components/title/Title';
import Button from '../../components/button/Button';

import style from './home.module.less';

const Home = () => (
    <div className={style.box}>
        <div className={style.container}>
            <Title>Your personal COOKBOOK</Title>
            <div className={style.description}>
                Available recipes:
            </div>
            <RecipesList/>
            <div className={style.addNewRecipe}>
                <Button href="/add-recipe" isLarge={true}>Add recipe</Button>
            </div>
        </div>
    </div>
);

export default Home;
