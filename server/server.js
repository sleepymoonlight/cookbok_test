const express = require('express');
const bodyParser = require('body-parser');
const JsonDB = require('node-json-db');
const cors = require('cors');

const db = new JsonDB("recipes", true, false);
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
let recipes = db.getData('/recipes');


app.get('/', (req, res) => res.send('Server API'));

//get all recipes

app.get('/recipes', (req, res) => res.send(recipes.filter(el => !el.childOf)));

//get current recipe

app.get('/recipes/:id', (req, res) => {
    const recipe = recipes.find(recipe => recipe.id === Number(req.params.id));
    const childrenRecipes = recipes.filter(recipe => recipe.childOf === Number(req.params.id));

    recipe.childrenRecipes = childrenRecipes;
    res.send(recipe);
});

//delete recipe

app.delete('/recipes/:id', (req, res) => {
    const recipes = db.getData('/recipes');
    const recipeIndex = recipes.findIndex(recipe => recipe.id === Number(req.params.id));

    db.delete(`/recipes[${recipeIndex}]`);

    res.send(recipes);
});

//add new recipe

app.post('/recipes', (req, res) => {
    const now = new Date();
    const date = `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()}  ${now.getHours()}:${now.getMinutes()}`;

    const recipe = {
        id: Date.now(),
        title: req.body.title,
        description: req.body.description,
        dateCreated: date,
        dateChanged: date,
    };

    if (req.body.childOf) {
        recipe.childOf = Number(req.body.childOf);
    }

    db.push('/recipes[]', recipe, true);
    res.send(recipe);
});

//edit recipe

app.put('/recipes/:id', (req, res) => {
    const recipes = db.getData('/recipes');
    const recipeIndex = recipes.findIndex(recipe => recipe.id === Number(req.params.id));
    const currentRecipe = recipes.find(recipe => recipe.id === Number(req.params.id));
    const now = new Date();
    const date = `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()}  ${now.getHours()}:${now.getMinutes()}`;
    const temp = {
        id: Date.now(),
        title: currentRecipe.title,
        description: currentRecipe.description,
        dateCreated: currentRecipe.dateCreated,
        dateChanged: date
    };

    if (recipeIndex >= 0) {
        db.delete(`/recipes[${recipeIndex}]`);
    }

    const previousVersions = currentRecipe.previousVersions || [];
    previousVersions.push(temp);

    const recipe = {
        id: Number(req.params.id),
        title: req.body.title,
        description: req.body.description,
        dateCreated: date,
        dateChanged: date,
        previousVersions: previousVersions
    };

    db.push('/recipes[]', recipe);
    res.send(recipes);
});


app.listen(8000, () => console.log(`Listening on port 8000`));
